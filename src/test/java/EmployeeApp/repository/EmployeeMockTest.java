package EmployeeApp.repository;

import EmployeeApp.controller.DidacticFunction;

import EmployeeApp.model.Employee;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {


    //test cu date valide
    @Test
    void modifyEmployeeFunction_TC1() {
        EmployeeMock employeeMock= new EmployeeMock();

        Employee oldEmployee = employeeMock.findEmployeeById(2);
        employeeMock.modifyEmployeeFunction(oldEmployee, DidacticFunction.ASISTENT);

        assertEquals(oldEmployee, employeeMock.findEmployeeById(2));
    }


    //test pentru lipsa employee
    @Test
    void modifyEmployeeFunction_TC3() {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee newEmployee = null;
        employeeMock.modifyEmployeeFunction(newEmployee, DidacticFunction.CONFERENTIAR);
        assertEquals(employeeList, employeeMock.getEmployeeList());

    }
}