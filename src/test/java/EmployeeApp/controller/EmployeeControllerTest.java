package EmployeeApp.controller;

import EmployeeApp.model.Employee;
import EmployeeApp.repository.EmployeeImpl;
import EmployeeApp.repository.EmployeeRepositoryInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {
    EmployeeController employeeController;

    @BeforeEach
    void setupO() {

        EmployeeRepositoryInterface employeeRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeeRepository);
    }
  //test cu date valide pentru adaugarea unui angajat
    @Test
    void addEmployeeTC1_BB() {

        try {
            int nr_empl = employeeController.getEmployeesList().size();//cati angajati au fost initial
            Employee e1 = new Employee();
            e1.setFirstName("Anca");
            e1.setLastName("Marinescu");
            e1.setCnp("2548615424400");
            e1.setFunction(DidacticFunction.CONFERENTIAR);
            e1.setSalary(10.000);
            try {
                employeeController.addEmployee(e1);
                assertEquals(nr_empl + 1, employeeController.getEmployeesList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception e) {
                e.printStackTrace();
                assert (false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //test cu date nevalide ce returneaza false la adaugarea unui angajat cu date incorecte
    @Test
    void addEmployeeTC2_BB() {

        try {
            int nr_empl = employeeController.getEmployeesList().size();//cati angajati au fost initial
            Employee e1 = new Employee();
            e1.setFirstName("Anca");
            e1.setLastName(null);
            e1.setCnp("2548615424400");
            e1.setFunction(DidacticFunction.CONFERENTIAR);
            e1.setSalary(10.000);
            try {
                employeeController.addEmployee(e1);
                assert (false);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("exceptie");
                assert (true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //test cu date valide - TC3
    @Test
    void addEmployeeTC3_BB() {

        try {
            int nr_empl = employeeController.getEmployeesList().size();//cati angajati au fost initial
            Employee e1 = new Employee();
            e1.setFirstName("Anca");
            e1.setLastName("Marinescu");
            e1.setCnp("2548615424400");
            e1.setFunction(DidacticFunction.CONFERENTIAR);
            e1.setSalary(9.000);
            try {
                employeeController.addEmployee(e1);
                assertEquals(nr_empl + 1, employeeController.getEmployeesList().size());
                System.out.println("Angajat adaugat cu succes");
            } catch (Exception e) {
                e.printStackTrace();
                assert (false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //test cu date nevalide -TC4
    @Test
    void addEmployeeTC4_BB() {

        try {
            int nr_empl = employeeController.getEmployeesList().size();//cati angajati au fost initial
            Employee e1 = new Employee();
            e1.setFirstName("Ac");
            e1.setLastName("M");
            e1.setCnp("2548615424400");
            e1.setFunction(DidacticFunction.CONFERENTIAR);
            e1.setSalary(10.000);
            try {
                employeeController.addEmployee(e1);
                assert (false);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("exceptie");
                assert (true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

